# Local Storage

|№|Key|Value|Description|Source|
|----------|----------|----------|----------|----------|
|1|_ym1937671_il|"Монитор Rombica SkyView M23-MF (MUT-002)"|Сохраняет карточку последнего товара|www.eldorado.ru|
|2|digiLastSearch|iphone 14|Сохраняет Введенные данные в строке поиска|www.eldorado.ru|
|3|tmr_reqNum|29|Счетчик кол-ва переходов по ссылкам на странице|ok.ru|
|4|wb_delivery-points-_d772069d-7766-44b0-9702-826577501dba|{"self":[{"sberPostamat":false,"partnerPost":false,"isFranchise":false,"deliveryName":"Пункт выдачи","pointTypeName":"Пункт выдачи","fittingPrice":0,"deliveryPrice":0,"zoneDeliveryPrice":0,"addressId":"103782","country":"ru","address":"Долгопрудный Новое шоссе 12","lat":"55.971722","lon":"37.510388","deliveryType":"self","isExternalPostamat":false,"workTime":"Ежедневно 08:00-22:00","deliveryIsUnavailable":false,"status":0,"rate":4.89},{"sberPostamat":false,"partnerPost":false,"isFranchise":false,"deliveryName":"Пункт выдачи","pointTypeName":"Пункт выдачи","fittingPrice":0,"deliveryPrice":0,"zoneDeliveryPrice":0,"addressId":"765","country":"ru","address":"Москва, Москва, Челюскинская улица, 11","lat":"55.883255","lon":"37.706005","deliveryType":"self","isExternalPostamat":false,"workTime":"Ежедневно 10:00-21:00","deliveryIsUnavailable":false,"status":0,"rate":5}],"courier":[]}|Описывает пункт и тип доставки|www.wildberries.ru|
|5|yt-player-quality|{"data":"{\"quality\":480,\"previousQuality\":360}","expiration":1720284607350,"creation":1689180607350}|Сохраняет настройки качества видео|www.youtube.com|


# Session Storage

|№|Key|Value|Description|Source|
|----------|----------|----------|----------|----------|
|1|UXS_updated_time|1689168003853|UXS_updated_time» потенциально может относиться к последнему времени обновления или изменения сеанса взаимодействия с пользователем (UX).|www.eldorado.ru|
|2|_slsession|5092DC92-775D-4637-A1AB-2073053CE78C|Потенциально это может относиться к сеансу, связанному с соглашениями об уровне обслуживания (SLA), или к идентификатору сеанса в конкретном программном обеспечении или системе.|www.eldorado.ru|
|3|sessionId|5062651689178105154|Может опознать действия, предпринятые на сайте. Например, отображать "последние просмотренные страницы", сохранять в интернет-магазине выбранные товары в корзине покупателя и предотвращать чужие действия под видом именно этого посетителя|ok.ru|
|4|spa_history|[{"state":{"name":"SpaBasketEntrypoint","id":"0690b853-e3eb-b4f2-1498-ffb557e94d6d","title":""}}]|Сохраняет историю перехода по элементам страницы|www.wildberries.ru|
|5|yt-player-autonavstate|{"data":"2","creation":1689180212337}|Сохраняет данные последнего открытого ролика|www.youtube.com|


# Результат выполнения команд для добавления и получения объекта в "Session Storage"

![](/misc/images/SessionStorage1.png)

# Состояние "Session Storage" после добавления объекта

![](/misc/images/SessionStorage2.png)

# Результат выполнения команд для добавления и получения объекта в "Local Storage"

![](/misc/images/LocalStorage1.png)

# Состояние "Local Storage" после добавления объекта

![](/misc/images/LocalStorage2.png)